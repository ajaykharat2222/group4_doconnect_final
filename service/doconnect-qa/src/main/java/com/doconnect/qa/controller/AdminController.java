package com.doconnect.qa.controller;

import com.doconnect.qa.model.Admin;
import com.doconnect.qa.service.AdminService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AdminController {
    @Autowired
    private AdminService adminservice;

    @PostMapping("/adminlogin")
    private String login(@RequestBody HashMap<String, String> body) {
        String email = body.get("email");
        String password = body.get("password");
        if (adminservice.isExists(email, password)) {
            return "ok";
        } else {
            return "no";
        }
    }
    
    @GetMapping("/adminemail/{email}")
    private Admin getAdminByEmail(@PathVariable String email) {
        return adminservice.findByEmail(email);
    }

    @PostMapping("/adminregister")
    private String Register(@RequestBody Admin u) {
        if (adminservice.isExists(u.getEmail(), u.getPassword())) {
            return "no";
        } else {
            adminservice.save(u);
            return "ok";
        }
    }

    @GetMapping("/admin/{id}")
    private Admin admin(@PathVariable int id) {
        try {
            return adminservice.getById(id);
        } catch (Exception e) {
            return new Admin();
        }
    }

    @GetMapping("/admins/unique/{name}")
    private List<String> getUniqueAdmins(@PathVariable String name) {
        return adminservice.getByDistinct(name);
    }

    @DeleteMapping("/deleteadmin/{email}")
    private String deleteAdmin(@PathVariable String email) {
        if (adminservice.findByEmail(email) != null) {
            adminservice.delete(email);
            return "ok";
        }
        return "no";
    }
}
