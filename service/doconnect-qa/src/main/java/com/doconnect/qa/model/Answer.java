package com.doconnect.qa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import net.bytebuddy.implementation.bind.annotation.Default;

@Entity
public class Answer {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String answer;
    private String topic;
    
    @OneToOne
    private Question question;
    
    @OneToOne
    private User user;
    
    
	public Answer() {
    	
    }
    
    public Answer(int id, String answer, String topic, Question question, User user) {
		super();
		this.id = id;
		this.answer = answer;
		this.topic = topic;
		this.question = question;
		this.user = user;
	}

	public int getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	
}
