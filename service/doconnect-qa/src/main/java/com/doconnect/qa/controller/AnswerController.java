package com.doconnect.qa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.doconnect.qa.model.Answer;
import com.doconnect.qa.model.Question;
import com.doconnect.qa.service.AnswerService;
import com.doconnect.qa.service.QuestionService;
import com.doconnect.qa.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AnswerController {
	@Autowired
    private QuestionService questionService;
    
    @Autowired
    private AnswerService answerService;
    
    @Autowired
    private UserService userService;
    
    @GetMapping("/answers")
    private List<Answer> getAnswers() {
        return answerService.getAll();
    }
 
    @GetMapping("/answer/{id}")
    private Answer getAnswer(@PathVariable int id) {
        try {
            return answerService.getById(id);
        } catch (Exception e) {
            return new Answer();
        }
    }
    
    @GetMapping("/answersbytopic/{topic}")
    private List<Answer> getAnswersByTopic(@PathVariable String topic) {
        return answerService.getByTopic(topic);
    }

}
