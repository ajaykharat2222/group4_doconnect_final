package com.doconnect.qa.service;

import com.doconnect.qa.model.Admin;
import com.doconnect.qa.repository.AdminRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Id;
import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepository;

    public boolean isExists(String email, String password) {
        return adminRepository.findByPassword(password) != null && adminRepository.findByEmail(email) != null;
    }

    public void save(Admin u) {
        adminRepository.save(u);
    }

    public void delete(String email) {
        Admin u = adminRepository.findByEmail(email);
        adminRepository.delete(u);
    }

    public void update(Admin u) {
        adminRepository.save(u);
    }

    public Admin findByEmail(String email) {
        return adminRepository.findByEmail(email);
    }

    public Admin findByPassword(String password) {
        return adminRepository.findByPassword(password);
    }

    public Admin getById(int id) {
        return adminRepository.findById(id).get();
    }

    public List<String> getByDistinct(String name) {
        return adminRepository.getByUniqueAdmin(name);
    }

}
