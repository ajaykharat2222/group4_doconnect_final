package com.doconnect.qa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.doconnect.qa.model.Admin;

import java.util.List;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {
    Admin findByEmail(String email);

    Admin findByPassword(String password);

    @Query("SELECT u.name FROM Admin u WHERE u.name!=:name")
    List<String> getByUniqueAdmin(@Param("name") String name);
}
