import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AdminserviceService } from '../service/adminservice.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  constructor(
    private service: AdminserviceService,
    private router: Router,
    private bar: MatSnackBar
  ) {}
  email: string = '';
  password: string = '';
  log(): void {
    this.service
      .adminlogin({
        email: this.email,
        password: this.password,
      })
      .then((res) => {
        if (res === 'ok') {
          this.service
            .getAdminByEmail(this.email)
            .then((res) => {
              if (res !== null) {
                localStorage.setItem('name', res.name);
                localStorage.setItem('email', res.email);
              }
            })
            .then(() => this.router.navigate(['adminhome']));
        } else {
          this.bar.open('Please Check the credentials', 'Close', {
            duration: 3000,
          });
        }
      });
  }
  ngOnInit(): void {}
}

